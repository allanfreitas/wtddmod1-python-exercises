import unittest
from basic.string1 import donuts, both_ends, fix_start, mix_up


class BasicString1TestCase(unittest.TestCase):

    def test_donuts_equal_4(self):
        self.assertEqual(donuts(4), 'Number of donuts: 4')

    def test_donuts_equal_9(self):
        self.assertEqual(donuts(9), 'Number of donuts: 9')

    def test_donuts_is_many(self):
        self.assertEqual(donuts(10), 'Number of donuts: many')
        self.assertEqual(donuts(99), 'Number of donuts: many')

    def test_both_ends(self):
        self.assertEqual(both_ends('spring'), 'spng')
        self.assertEqual(both_ends('Hello'), 'Helo')

        self.assertEqual(both_ends('xyz'), 'xyyz')

    def test_both_ends_string_less_then_2(self):
        self.assertEqual(both_ends('a'), '')

    def test_fix_start(self):
        self.assertEqual(fix_start('babble'), 'ba**le')
        self.assertEqual(fix_start('aardvark'), 'a*rdv*rk')
        self.assertEqual(fix_start('google'), 'goo*le')
        self.assertEqual(fix_start('donut'), 'donut')

    def test_mix_up(self):
        self.assertEqual(mix_up('mix', 'pod'), 'pox mid')
        self.assertEqual(mix_up('dog', 'dinner'), 'dig donner')
        self.assertEqual(mix_up('gnash', 'sport'), 'spash gnort')
        self.assertEqual(mix_up('pezzy', 'firm'), 'fizzy perm')


if __name__ == '__main__':
    unittest.main()

# to Run this file only
# python -m unittest tests/basic/test_string1.py